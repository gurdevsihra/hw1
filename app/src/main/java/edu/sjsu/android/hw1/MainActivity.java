package edu.sjsu.android.hw1;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.EditText;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.RadioGroup;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {


    private EditText amount;
    private SeekBar interestRate;
    private TextView intro;
    private TextView monthlyPayment;
    private TextView progressText;
    private ProgressBar pbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        amount = (EditText) findViewById(R.id.editText);
        interestRate = (SeekBar) findViewById(R.id.seekBar);
        interestRate.setProgress(10);
        monthlyPayment = (TextView) findViewById(R.id.textView2);
        intro = (TextView) findViewById(R.id.textView3);
        progressText = (TextView) findViewById(R.id.textView4);
        pbar = (ProgressBar) findViewById(R.id.progressBar);

        interestRate.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {


            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                pbar.setProgress(progress);
                progressText.setText("" + progress + "%");
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                pbar.setProgress(10);
                progressText.setText("" +10 + "%");
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }



    public void onClick(View view) { switch (view.getId()) {
        case R.id.button:
            RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
            RadioButton fifteen = (RadioButton) findViewById(R.id.radioButton1);
            RadioButton twenty = (RadioButton) findViewById(R.id.radioButton2);
            //RadioButton thirty = (RadioButton) findViewById(R.id.radioButton3);
            CheckBox tax = (CheckBox) findViewById(R.id.checkBox);

            if (amount.getText().length() == 0 || radioGroup.getCheckedRadioButtonId() == -1) {
                Toast.makeText(this, "Please ensure that the amount and a year value is chosen. :(", Toast.LENGTH_LONG).show(); return;
            }
            int years = 0;
            if(fifteen.isChecked()){
                years = 15;
            }
            else if(twenty.isChecked()){
                years = 20;
            }else{
                years = 30;
            }
            float amountNum = Float.parseFloat(amount.getText().toString());
            float interestRateNum = interestRate.getProgress();
            intro.setText("Calculated Monthly Payment");
            if(tax.isChecked()){
                monthlyPayment.setText("$" + String.valueOf(calculateMortgageTaxInsur(amountNum, interestRateNum, years, true)));
            }else{
                monthlyPayment.setText("$" + String.valueOf(calculateMortgageTaxInsur(amountNum, interestRateNum, years, false)));
            }
            break;
        }
    }

    public static float calculateMortgageTaxInsur(float amount, float interest, int years, boolean taxAndInsurance) {

        float total = amount;

        float months = years * 12;

        if(interest == 0){
            total /= months;

        }else{
            float monthlyInterest =interest/1200;
            months *= -1;

            double val = ((monthlyInterest) /  (1 - Math.pow((1+monthlyInterest), months)));
            total *= val;
        }

        if(taxAndInsurance){
            double tax = amount - (amount * .01);
            total += tax;
        }
        return total;
    }

}
